/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package summarization;

/**
 *
 * @author Jario
 */
public class UpperCase {

    private boolean isCapitalLetter(String word) {
//        for (int i = word.length() - 1; i >= 0; i--) {
        if (Character.isUpperCase(word.charAt(0))) {
            return true;
        }
//        }
        return false;
    }

    public double computingUpperCase(String sentence) {

        String[] words = sentence.split(" ");
        int NTW = words.length;
        int NCW = 0;
        for (String word : words) {
            if (!word.isEmpty()) {
                if (isCapitalLetter(word)) {
                    NCW++;
                }
            }
        }
        double CPTW = (NCW+1) / (NTW+1);

        return CPTW;
    }
}
