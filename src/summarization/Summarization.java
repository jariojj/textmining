/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package summarization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jario
 */
public class Summarization {

    final int MAX_SENTENCES = 4;
    private Map<Integer, String> content;
    private List<String> sentences;
    private String title;
    private String line;
    private int blank = 0;
    private String local;

    public Summarization() {
    }

    public void loadFile(String dir) {

        File dirBaseP = new File(dir);

        for (String list : dirBaseP.list()) {

            BufferedReader lerArq = null;
            try {
                String f = dir + "/" + list;
                local = f;
                System.out.println(f);
                lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
                String linha = lerArq.readLine();
                int aux = 0;
                blank = 0;
                content = new HashMap<>();
                while (linha != null) {

                    //System.out.println(linha);
                    if (linha.contains("Subject: ")) {
                        title = linha;
                        title = title.replaceAll("Subject:", "");
                        title = title.replaceAll("Re:", "");
                        title = title.replaceAll(">", " ");
                    }
                    if (linha.contains("Lines:")) {
                        line = linha.replaceAll("Lines:", "");
                        line = line.replace(" ", "");
                        line = line.replace(">", "");
                    }
                    if (linha.equals("") || linha.equals(" ")) {
                        linha = lerArq.readLine();
                        blank++;
                        continue;
                    }
                    content.put(aux, linha);
                    aux++;

                    linha = lerArq.readLine();
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Summarization.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Summarization.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    lerArq.close();
                } catch (IOException ex) {
                    Logger.getLogger(Summarization.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //break;
            System.out.println("title: " + title);
            preProcess();

        }

//        System.out.println("title: " + title);
    }

    public void preProcess() {

        StringBuilder sb = new StringBuilder();
        sentences = new ArrayList<>();
//        String lastSentence;
//        System.out.println(line);
        Integer id = Integer.parseInt(line);
        id = id - blank + 1;
        System.out.println("id: " + id);

        for (int i = content.size() - id; i <= content.size() - 1; i++) {
//            System.out.println("Content: " + content.get(i));
            String sentence = content.get(i);
            if (sentence != null) {
                for (String caractere : Caracteres.getCaracteres()) {

                    if (caractere.equals("?")) {
                        sentence = sentence.replaceAll("[\\" + caractere + "]", " ");
//                                        System.out.println("LINHA_: " + linha);
                    } else {
//                            System.out.println(caractere);
//                            System.out.println(sentence);
                        sentence = sentence.replaceAll("[" + caractere + "]", " ");
//                                        System.out.println("LINHA: " + linha);
                    }
                }

//                System.out.println("SENTENCE: " + sentence);
                sb.append(sentence);
//                sentences.add(sentence);
            }

            /*
             * Agrupar as sentencas por pontos
             */
//            for(int j = 0; j < sentence.length(); j++){
//                if(sentence.charAt(j) == '.'){
//                    if(j+1 < sentence.length()){
//                        if(sentence.charAt(j+1) == ' '){
//                            sentences.add(sentence.substring(0, j));
//                            break;
//                        }else{
//                            lastSentence = sentence;
//                        }
//                    }
//                }
//            }
        }

        computingUpperCaseANDTitleSimilarity(sb.toString());
//        System.out.println("Text: " + sb.toString());
//        String[] s = sb.toString().split(".");
//        for(String st : s){
//            String[] w = st.split(" ");
//            
//        }

    }

    public void computingUpperCaseANDTitleSimilarity(String text) {

        FileWriter arqW = null;
        Map<String, Integer> or = new HashMap<>();
        List<String> stnc = new ArrayList<>();
        List<String> s = new ArrayList<>();
        int c = 0;
        try {
            System.out.println("TEXT: " + text);
            Map<String, Double> sS = new HashMap<>();
            double ucS = 0.0;
            double tsS = 0.0;
            String[] sen = text.split("[\\.?!]{1,} ?");
            System.out.println("LEN: " + sen.length);
            UpperCase uc = new UpperCase();
            TitleSimilarity ts = new TitleSimilarity(title);
            for (String sentence : sen) {
                if(sentence.length() < 10)
                    continue;
                System.out.println("SENTENCE: " + sentence);
                ucS = uc.computingUpperCase(sentence);
                String[] words = sentence.split(" ");
                for (String word : words) {
                    if(!word.equals(" ") && !word.equals("") && !StopWord.getStopwords().containsKey(word))
                        tsS += ts.computingSimilarity(word);
                }

                s.add(sentence);
                sS.put(sentence, (ucS + tsS));
                or.put(sentence, c);
                c++;
            }
            List ll = new LinkedList(); // Collections.sort() recebe como parametro um list
            ll.addAll(sS.values());// buscando os valores no Map
            Collections.sort(ll);

            arqW = new FileWriter(local + ".txt", true);

            PrintWriter gravarArq = new PrintWriter(arqW);

            int i = 0;
            for (Map.Entry<String, Double> entrySet : sS.entrySet()) {
                String key = entrySet.getKey();
                Double value = entrySet.getValue();

                System.out.println("key: " + key);
                System.out.println("Value: " + value);

                if (sS.size() > MAX_SENTENCES) {
//                    gravarArq.printf("%s", key+". ");
                    stnc.add(key);
                } else {
                    stnc.add(key);
//                    gravarArq.printf("%s", key+". ");
                    i--;
                }
                if(MAX_SENTENCES == i+1)
                    break;
                i++;
            }
                
            List<Integer> v = new ArrayList<>();
            for (String st : stnc) {
                v.add(or.get(st));
            }
            Collections.sort(v);
            for (Integer v1 : v) {
                gravarArq.printf("%s", s.get(v1)+". ");
            }
            arqW.close();
        } catch (IOException ex) {
            Logger.getLogger(Summarization.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                arqW.close();
            } catch (IOException ex) {
                Logger.getLogger(Summarization.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void main(String[] args) {

        String dir = "C:/Users/Jario/Documents/resumos";

        Summarization s = new Summarization();

        s.loadFile(dir);
        //s.preProcess();
    }
}
