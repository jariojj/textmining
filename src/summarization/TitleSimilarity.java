/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package summarization;

import edu.cmu.lti.jawjaw.JAWJAW;
import edu.cmu.lti.jawjaw.pobj.POS;
import edu.cmu.lti.ws4j.util.PorterStemmer;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Jario
 */
public class TitleSimilarity {

    private String title;
    private String[] titleStemming;
    private Map<String, Set<String>> synonym;

    public TitleSimilarity(String title) {
        synonym = new HashMap<>();

        this.title = title;
        titleStemming = stemming(title);
        for (String word : title.split(" ")) {
            word = word.replaceAll("[.]", " ");
            word = word.replaceAll("[,]", " ");
            word = word.replaceAll("[\\?]", " ");
            word = word.replaceAll("[!]", " ");
            word = word.replaceAll("[']", "");
            findSynonyms(word);
        }
    }

    private String[] stemming(String sentence) {
        String[] words = sentence.split(" ");

        int aux = 0;
        for (String w : words) {
            String word = w;
            PorterStemmer ps = new PorterStemmer();
            word = ps.stemWord(w);
            while (!word.equals(w)) {
                w = word;
                word = ps.stemWord(w);
            }
            words[aux] = word;
            aux++;
        }

//        for (String word : words) {
//            System.out.println(word);
//        }
        return words;
    }

    private void findSynonyms(String word) {

        for (POS value : POS.values()) {
            //System.out.println("VALUE: " + value);
            Set<String> set = JAWJAW.findSynonyms(word, value);
            //System.out.println(set);
            if (synonym.containsKey(word)) {
                Set<String> s = synonym.get(word);
                s.addAll(set);
            } else {
                synonym.put(word, set);
            }
        }
    }

    public double computingSimilarity(String word) {

        double similarity = 0.0;
        //Se a palavra esta contida no titulo
        String[] words = title.split(" ");
        for (String w : words) {
            if (word.equals(w)) {
                similarity = 1.0;
                break;
            }
        }
        if (similarity == 0.0) {
            //Se o radical da palavra esta contido no titulo
            String[] wS = stemming(word);
            for (String w : titleStemming) {
                if (wS[0].equals(w)) {
                    similarity = 0.6;
                    break;
                }
            }
        }
        if (similarity == 0.0) {
            //Se a palavra esta contida nos sinonimos do titulo
            for (Map.Entry<String, Set<String>> entrySet : synonym.entrySet()) {
                String key = entrySet.getKey();
                Set<String> value = entrySet.getValue();
                for (String w : value) {
                    if(word.equals(w)){
                        similarity = 0.4;
                    }
                }
            }
        }

        return similarity;
    }

//    public static void main(String[] args) {
//
//        String title = "relatedness words love";
//        TitleSimilarity ts = new TitleSimilarity(title.toLowerCase());
//        System.out.println("Compute Similarity: " + ts.computingSimilarity("love"));
//    }
}
