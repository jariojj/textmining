/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paradigmatic;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jario
 */
public class Caracteres {
    
    private static List<String> carac;
    private static Boolean check = false;
    
    private static void loadCaracteres(){
        
        carac = new ArrayList<>();
        carac.add("!");
        carac.add(":");
        carac.add(",");
        carac.add(".");
        carac.add(";");
        carac.add("?");
        carac.add("'");
        carac.add("\"");
    }
    public static List<String> getCaracteres(){
        
        if(!check){
            loadCaracteres();
            check = true;
        }
        return carac;
    }
}
