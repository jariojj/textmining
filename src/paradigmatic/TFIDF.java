/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paradigmatic;

/**
 *
 * @author Jario
 */
public class TFIDF {
      /*
     O calculo do TFIDF e dado da seguinte maneira:
        Se temos um documento e nele aparece uma palavra gato 3x e o mesmo 
        documento possui 10 palavras, o TF = 3/10
        Se temos 10000 documentos e a palavra gato apareceu em 10 deles, o
        IDF = log(10000/10), logo, ITIDF = TF * IDF;
    */
    public static Double calculoTFIDF(Double palavraPorDoc, Double qtddPalavrasDoc, Double qtddDoc, Double palavraPorDocs){
        
//        System.out.println(palavraPorDoc);
//        System.out.println(qtddPalavrasDoc);
//        System.out.println(qtddDoc);
//        System.out.println(palavraPorDocs);
//        System.out.println("-------------------");
        //Double TF = Math.log(1 + palavraPorDoc);
        Double TF = palavraPorDoc/qtddPalavrasDoc;
        Double IDF = Math.log((1+qtddDoc)/palavraPorDocs);
        
        return TF*IDF;
    }
}
