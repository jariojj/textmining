/*
 *Tochangethislicenseheader,chooseLicenseHeadersinProjectProperties.
 *Tochangethistemplatefile,chooseTools|Templates
 *andopenthetemplateintheeditor.
 */
package paradigmatic;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @authorJario
 */
public class StopWord {

    private static Boolean check = false;
    private static Map<String, String> stopWords;

    private static Map<String, String> loadStopWords() {
        stopWords = new HashMap<>();

        stopWords.put("de", "");
        stopWords.put("a", "");
        stopWords.put("o", "");
        stopWords.put("que", "");
        stopWords.put("e", "");
        stopWords.put("do", "");
        stopWords.put("da", "");
        stopWords.put("em", "");
        stopWords.put("um", "");
        stopWords.put("para", "");
        stopWords.put("é", "");
        stopWords.put("com", "");
        stopWords.put("não", "");
        stopWords.put("uma", "");
        stopWords.put("os", "");
        stopWords.put("no", "");
        stopWords.put("se", "");
        stopWords.put("na", "");
        stopWords.put("por", "");
        stopWords.put("mais", "");
        stopWords.put("as", "");
        stopWords.put("dos", "");
        stopWords.put("como", "");
        stopWords.put("mas", "");
        stopWords.put("foi", "");
        stopWords.put("ao", "");
        stopWords.put("ele", "");
        stopWords.put("das", "");
        stopWords.put("tem", "");
        stopWords.put("à", "");
        stopWords.put("seu", "");
        stopWords.put("sua", "");
        stopWords.put("ou", "");
        stopWords.put("ser", "");
        stopWords.put("quando", "");
        stopWords.put("muito", "");
        stopWords.put("há", "");
        stopWords.put("nos", "");
        stopWords.put("já", "");
        stopWords.put("está", "");
        stopWords.put("eu", "");
        stopWords.put("também", "");
        stopWords.put("só", "");
        stopWords.put("pelo", "");
        stopWords.put("pela", "");
        stopWords.put("até", "");
        stopWords.put("isso", "");
        stopWords.put("ela", "");
        stopWords.put("entre", "");
        stopWords.put("era", "");
        stopWords.put("depois", "");
        stopWords.put("sem", "");
        stopWords.put("mesmo", "");
        stopWords.put("aos", "");
        stopWords.put("ter", "");
        stopWords.put("seus", "");
        stopWords.put("quem", "");
        stopWords.put("nas", "");
        stopWords.put("me", "");
        stopWords.put("esse", "");
        stopWords.put("eles", "");
        stopWords.put("estão", "");
        stopWords.put("você", "");
        stopWords.put("tinha", "");
        stopWords.put("foram", "");
        stopWords.put("essa", "");
        stopWords.put("num", "");
        stopWords.put("nem", "");
        stopWords.put("suas", "");
        stopWords.put("meu", "");
        stopWords.put("às", "");
        stopWords.put("minha", "");
        stopWords.put("têm", "");
        stopWords.put("numa", "");
        stopWords.put("pelos", "");
        stopWords.put("elas", "");
        stopWords.put("havia", "");
        stopWords.put("seja", "");
        stopWords.put("qual", "");
        stopWords.put("será", "");
        stopWords.put("nós", "");
        stopWords.put("tenho", "");
        stopWords.put("lhe", "");
        stopWords.put("deles", "");
        stopWords.put("essas", "");
        stopWords.put("esses", "");
        stopWords.put("pelas", "");
        stopWords.put("este", "");
        stopWords.put("fosse", "");
        stopWords.put("dele", "");
        stopWords.put("tu", "");
        stopWords.put("te", "");
        stopWords.put("vocês", "");
        stopWords.put("vos", "");
        stopWords.put("lhes", "");
        stopWords.put("meus", "");
        stopWords.put("minhas", "");
        stopWords.put("teu", "");
        stopWords.put("tua", "");
        stopWords.put("teus", "");
        stopWords.put("tuas", "");
        stopWords.put("nosso", "");
        stopWords.put("nossa", "");
        stopWords.put("nossos", "");
        stopWords.put("nossas", "");
        stopWords.put("dela", "");
        stopWords.put("delas", "");
        stopWords.put("esta", "");
        stopWords.put("estes", "");
        stopWords.put("estas", "");
        stopWords.put("aquele", "");
        stopWords.put("aquela", "");
        stopWords.put("aqueles", "");
        stopWords.put("aquelas", "");
        stopWords.put("isto", "");
        stopWords.put("aquilo", "");
        stopWords.put("estou", "");
        stopWords.put("está", "");
        stopWords.put("estamos", "");
        stopWords.put("estão", "");
        stopWords.put("estive", "");
        stopWords.put("esteve", "");
        stopWords.put("estivemos", "");
        stopWords.put("estiveram", "");
        stopWords.put("estava", "");
        stopWords.put("estávamos", "");
        stopWords.put("estavam", "");
        stopWords.put("estivera", "");
        stopWords.put("estivéramos", "");
        stopWords.put("esteja", "");
        stopWords.put("estejamos", "");
        stopWords.put("estejam", "");
        stopWords.put("estivesse", "");
        stopWords.put("estivéssemos", "");
        stopWords.put("estivessem", "");
        stopWords.put("estiver", "");
        stopWords.put("estivermos", "");
        stopWords.put("estiverem", "");
        stopWords.put("hei", "");
        stopWords.put("há", "");
        stopWords.put("havemos", "");
        stopWords.put("hão", "");
        stopWords.put("houve", "");
        stopWords.put("houvemos", "");
        stopWords.put("houveram", "");
        stopWords.put("houvera", "");
        stopWords.put("houvéramos", "");
        stopWords.put("haja", "");
        stopWords.put("hajamos", "");
        stopWords.put("hajam", "");
        stopWords.put("houvesse", "");
        stopWords.put("houvéssemos", "");
        stopWords.put("houvessem", "");
        stopWords.put("houver", "");
        stopWords.put("houvermos", "");
        stopWords.put("houverem", "");
        stopWords.put("houverei", "");
        stopWords.put("houverá", "");
        stopWords.put("houveremos", "");
        stopWords.put("houverão", "");
        stopWords.put("houveria", "");
        stopWords.put("houveríamos", "");
        stopWords.put("houveriam", "");
        stopWords.put("sou", "");
        stopWords.put("somos", "");
        stopWords.put("era", "");
        stopWords.put("éramos", "");
        stopWords.put("eram", "");
        stopWords.put("fui", "");
        stopWords.put("foi", "");
        stopWords.put("fomos", "");
        stopWords.put("foram", "");
        stopWords.put("fora", "");
        stopWords.put("fôramos", "");
        stopWords.put("seja", "");
        stopWords.put("sejamos", "");
        stopWords.put("sejam", "");
        stopWords.put("fosse", "");
        stopWords.put("fôssemos", "");
        stopWords.put("fossem", "");
        stopWords.put("for", "");
        stopWords.put("formos", "");
        stopWords.put("forem", "");
        stopWords.put("serei", "");
        stopWords.put("será", "");
        stopWords.put("seremos", "");
        stopWords.put("serão", "");
        stopWords.put("seria", "");
        stopWords.put("seríamos", "");
        stopWords.put("seriam", "");
        stopWords.put("tenho", "");
        stopWords.put("tem", "");
        stopWords.put("temos", "");
        stopWords.put("tém", "");
        stopWords.put("tinha", "");
        stopWords.put("tínhamos", "");
        stopWords.put("tinham", "");
        stopWords.put("tive", "");
        stopWords.put("teve", "");
        stopWords.put("tivemos", "");
        stopWords.put("tiveram", "");
        stopWords.put("tivera", "");
        stopWords.put("tivéramos", "");
        stopWords.put("tenha", "");
        stopWords.put("tenhamos", "");
        stopWords.put("tenham", "");
        stopWords.put("tivesse", "");
        stopWords.put("tivéssemos", "");
        stopWords.put("tivessem", "");
        stopWords.put("tiver", "");
        stopWords.put("tivermos", "");
        stopWords.put("tiverem", "");
        stopWords.put("terei", "");
        stopWords.put("terá", "");
        stopWords.put("teremos", "");
        stopWords.put("terão", "");
        stopWords.put("teria", "");
        stopWords.put("teríamos", "");
        stopWords.put("teriam", "");

        return stopWords;
    }

    public static Map<String, String> getStopwords() {

        if (!check) {
            loadStopWords();
            check = true;
        }
        return stopWords;
    }
}
