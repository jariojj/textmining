/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paradigmatic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jario
 */
public class Paradigmatic {

    private Integer quantidadeDoc = 0;
    private List<String> chaves;
    private Map<String, List<Integer>> palavraDocs;
    private Map<String, Map<Integer, Integer>> docPalavra;
    private List<String> documentos;
    private MatrizFrequencia matrizFrequencia;
    private Map<Integer, Integer> palavrasPorDoc;
    private Map<String, Set<String>> esq;
    private Map<String, Set<String>> dir;
    String dirBase = "";

    public Paradigmatic() {

        docPalavra = new HashMap<>();
        palavrasPorDoc = new HashMap<>();
        palavraDocs = new HashMap<>();
        documentos = new ArrayList<>();
        chaves = new ArrayList<>();
        esq = new HashMap<>();
        dir = new HashMap<>();
    }

    public void preT() {

        try {
            File dirBaseP = new File(dirBase);

            FileReader arq = new FileReader(dirBaseP);

            BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(dirBaseP), "ISO-8859-1"));
            String linha = lerArq.readLine();

            while (linha != null) {

                System.out.println(linha);

                if (!linha.equals("") && !linha.equals(" ")) {
                    for (String caractere : Caracteres.getCaracteres()) {

                        if (caractere.equals("?")) {
                            linha = linha.replaceAll("[\\" + caractere + "]", "");
//                                        System.out.println("LINHA_: " + linha);
                        } else {
                            linha = linha.replaceAll("[" + caractere + "]", "");
//                                        System.out.println("LINHA: " + linha);
                        }
                    }

                    String[] palavras = linha.split(" ");

                    String esquerda = "_esquerda_";
                    String direita = "_direita_";
                    String pivo = "_pivo_";
                    List<String> ESQDIR = new ArrayList<>();
                    int aux = 0;
                    int count = 0;
                    for (String p : palavras) {

                        p = p.toLowerCase();

                        if (this.palavraDocs.containsKey(p)) {
                            List l = palavraDocs.get(p);
                            l.add(quantidadeDoc);
                            this.palavraDocs.put(p, l);
                            if (count == 0) {
                                count++;
                            }
                            pivo = direita;
                            direita = esquerda;
                            esquerda = p;
                        } else if (StopWord.getStopwords().containsKey(p)) {

                        } else {

                            List<Integer> l = new ArrayList<>();
                            l.add(quantidadeDoc);
                            this.palavraDocs.put(p, l);
                            chaves.add(p);
                            count++;
                            pivo = direita;
                            direita = esquerda;

                            esquerda = p;

//                            ESQDIR.add(p);
                        }
                        if (docPalavra.containsKey(p)) {
                            Map<Integer, Integer> m = docPalavra.get(p);
                            m.put(quantidadeDoc, count);
                            docPalavra.put(p, m);
                        } else {
                            Map<Integer, Integer> m = new HashMap<>();
                            m.put(quantidadeDoc, count);
                            docPalavra.put(p, m);
                        }

                        if (!direita.equals("_pivo_") && !direita.equals("_direita_") && !direita.equals("_esquerda_") && !StopWord.getStopwords().containsKey(esquerda)) {
                            if (dir.containsKey(direita)) {
                                Set<String> d = dir.get(direita);
                                d.add(esquerda);
                            } else {
                                Set<String> d = new HashSet<>();
                                d.add(direita);
                                dir.put(esquerda, d);
                            }
                        }
                        if (!pivo.equals("_esquerda_") && !pivo.equals("_direita_") && !pivo.equals("_pivo_") && !StopWord.getStopwords().containsKey(pivo)) {
                            if (esq.containsKey(direita)) {
                                Set<String> d = esq.get(direita);
                                d.add(pivo);
                            } else {
                                Set<String> d = new HashSet<>();
                                d.add(pivo);
                                esq.put(direita, d);
                            }
                        }

                    }

//                    for (int i = 1; i < ESQDIR.size(); i++) {
//                        System.out.println("SIZE: " + ESQDIR.size());
////                        for (int j = 1; j < i; j++) {
//                            if (esq.containsKey(ESQDIR.get(i))) {
//                                List<String> l = esq.get(ESQDIR.get(i));
//                                l.add(ESQDIR.get(i - 1));
//                                esq.put(ESQDIR.get(i), l);
//                            } else {
//                                List<String> l = new ArrayList<>();
//                                l.add(ESQDIR.get(i - 1));
//                                esq.put(ESQDIR.get(i), l);
//                            }
//                        }
//                        for (int j = 0; j < i; j++) {
//                            if (dir.containsKey(ESQDIR.get(i))) {
//                                List<String> l = esq.get(ESQDIR.get(i));
//                                l.add(ESQDIR.get(i));
//                                dir.put(ESQDIR.get(i), l);
//                            } else {
//                                List<String> l = new ArrayList<>();
//                                l.add(ESQDIR.get(i + 1));
//                                dir.put(ESQDIR.get(i), l);
//
//                            }
//                        }
//                    }
//                   
//                    for (int i = 0; i < ESQDIR.size()-1; i++) {
//                        System.out.println("SIZE: " + ESQDIR.size());
//                        for (int j = 1; j < i; j++) {
//                            if (esq.containsKey(ESQDIR.get(i))) {
//                                List<String> l = esq.get(ESQDIR.get(i));
//                                l.add(ESQDIR.get(i - 1));
//                                esq.put(ESQDIR.get(i), l);
//                            } else {
//                                List<String> l = new ArrayList<>();
//                                l.add(ESQDIR.get(i - 1));
//                                esq.put(ESQDIR.get(i), l);
//                            }
//                        }
//                        for (int j = 0; j < i; j++) {
//                            if (dir.containsKey(ESQDIR.get(i))) {
//                                List<String> l = esq.get(ESQDIR.get(i));
//                                l.add(ESQDIR.get(i));
//                                dir.put(ESQDIR.get(i), l);
//                            } else {
//                                List<String> l = new ArrayList<>();
//                                l.add(ESQDIR.get(i + 1));
//                                dir.put(ESQDIR.get(i), l);
//
//                            }
////                        }
//                    
                    palavrasPorDoc.put(quantidadeDoc, count);
                    quantidadeDoc++;
                    documentos.add(linha);
                }

                linha = lerArq.readLine();
            }

            arq.close();

//            for (Map.Entry<String, List<String>> entrySet : esq.entrySet()) {
//                String key = entrySet.getKey();
//                List<String> value = entrySet.getValue();
//                System.out.print(key + ": ");
//                for (String value1 : value) {
//                    System.out.print(value1 + " ");
//                }
//                System.out.println("");
//            }
            for (Map.Entry<String, List<Integer>> entrySet : palavraDocs.entrySet()) {
                String key = entrySet.getKey();
                List<Integer> value = entrySet.getValue();

                System.out.print(key + ": ");
                for (Iterator<Integer> iterator = value.iterator(); iterator.hasNext();) {
                    Integer next = iterator.next();

                    System.out.print(next + " ");
                }
                System.out.println("");
            }

        } catch (IOException ex) {
            Logger.getLogger(Paradigmatic.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void construirMatrizFrequencia() {

        matrizFrequencia = new MatrizFrequencia(palavraDocs.size(), quantidadeDoc);
        int aux = 0;

        for (String c : chaves) {

            List<Integer> value = palavraDocs.get(c);

            for (Integer vl : value) {

                double v = matrizFrequencia.get(aux, vl) + 1.0;
                matrizFrequencia.set(aux, vl, v);
            }
            aux++;
        }

        matrizFrequencia.print(0, 3);
    }

    public void calcularTFIDF() {

        int i = 0;
        int qtdd = 0;
        for (String c : chaves) {

            for (int j = 0; j < quantidadeDoc; j++) {

//                System.out.println(matrizFrequencia.get(i, j));
//                System.out.println((double) palavrasPorDoc.get(j));
//                System.out.println((double) quantidadeDoc);
//                System.out.println((double) palavraDocs.get(chaves.get(i)).size());
                System.out.println("Chave: " + c);
                matrizFrequencia.set(i, j, TFIDF.calculoTFIDF(matrizFrequencia.get(i, j), (double) palavrasPorDoc.get(j), (double) quantidadeDoc, (double) docPalavra.get(c).size()));
            }
            i++;
        }
        System.out.println("");
        System.out.println("");
        System.out.println("");
        matrizFrequencia.print(0, 3);
    }

    public void calcularSimilaridade(String w1, String w2) {

        Set<String> le1 = esq.get(w1);
        System.out.print("Esquerda " + w1 + ": ");
        for (String le11 : le1) {
            System.out.print(le11 + " ");
        }
        System.out.println("");
        System.out.print("Direita " + w1 + ": ");
        Set<String> ld1 = dir.get(w1);
        for (String ld11 : ld1) {
            System.out.print(ld11 + " ");
        }
        System.out.println("");

        System.out.print("Esquerda " + w2 + ": ");
        Set<String> le2 = esq.get(w2);
        for (String le21 : le2) {
            System.out.print(le21 + " ");
        }
        System.out.println("");
        System.out.print("Direita " + w2 + ": ");
        Set<String> ld2 = dir.get(w2);
        for (String ld21 : ld2) {
            System.out.print(ld21 + " ");
        }
        System.out.println("");

        Set<String> sDw1 = dir.get(w1);
        Set<String> sDw2 = dir.get(w2);

        Set<String> sWD = new HashSet<>();
        for (String sdw1 : sDw1) {

            for (String sdw2 : sDw2) {
                if (sdw1.equals(sdw2)) {
                    sWD.add(sdw2);
                }
            }
        }

        double simD = 0.0;
        int i = 0;
        int j = 0;
        for (String c : chaves) {
            j = 0;
            for (String sdw1 : sWD) {
                if (c.equals(sdw1)) {
                    System.out.println(i + " " + j);
                    simD += matrizFrequencia.get(i, j);
                }

                j++;

                if (j > quantidadeDoc) {
                    break;
                }
            }

            i++;

            if (i > chaves.size()) {
                break;
            }
        }

        Set<String> sEw1 = esq.get(w1);
        Set<String> sEw2 = esq.get(w2);

        Set<String> sWE = new HashSet<>();
        for (String sew1 : sEw1) {

            for (String sew2 : sEw2) {
                if (sew1.equals(sew2)) {
                    sWE.add(sew2);
                }
            }
        }

        double simE = 0.0;
        int a = 0;
        int b = 0;
        for (String c : chaves) {

            for (String sew1 : sWE) {
                if (c.equals(sew1)) {
                    simE += matrizFrequencia.get(a, b);
                }
                b++;
                if (b > quantidadeDoc) {
                    break;
                }
            }

            a++;

            if (a > chaves.size()) {
                break;
            }
        }

        double sim = (simE + simD)*(sWD.size()+sWE.size());
        
        if(sim > 1.0){
            System.out.println("Totalmente similar");
        }else{
            System.out.println("Similaridade: " + sim);
        }
    }

    public static void main(String[] args) {
        Paradigmatic pp = new Paradigmatic();
        pp.dirBase = "Dataset.txt";

        String w1 = "microsoft";
        String w2 = "apple";

        pp.preT();
        pp.construirMatrizFrequencia();
        pp.calcularTFIDF();

        pp.calcularSimilaridade(w1, w2);
    }
}
