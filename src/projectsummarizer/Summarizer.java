/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectsummarizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import paradigmatic.MatrizFrequencia;
import paradigmatic.TFIDF;
import summarization.Caracteres;
import summarization.StopWord;
import summarization.TitleSimilarity;
import summarization.UpperCase;

/**
 *
 * @author Jario
 */
public class Summarizer {

    final int MAX_SENTENCES = 4;
    private String local;

    private Integer quantidadeDoc = 0;
    private List<String> chaves;
    private List<String> content;
    private Map<String, List<Integer>> palavraDocs;
    private MatrizFrequencia matrizFrequencia;
    private Map<Integer, Integer> palavrasPorDoc;
    private Map<String, Map<Integer, Integer>> docPalavra;

    public Summarizer() {
    }

    public void loadFile(String dir) {

        File dirBaseP = new File(dir);

        for (String listV : dirBaseP.list()) {
            System.out.println(dir + "/" + listV);
            File dirBaseV = new File(dir + "/" + listV);
            if (!dirBaseV.isDirectory()) {
                continue;
            }
            for (String list : dirBaseV.list()) {
                if (list.contains(".txt")) {

                    this.palavraDocs = new HashMap<>();
                    this.chaves = new ArrayList<>();
                    this.palavrasPorDoc = new HashMap<>();
                    this.docPalavra = new HashMap<>();
                    this.content = new ArrayList<>();
                    quantidadeDoc = 0;

                    BufferedReader lerArq = null;
                    try {
                        String f = dirBaseV + "/" + list;
                        local = f;
                        System.out.println(f);
                        lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
                        String linha = lerArq.readLine();
                        int aux = 0;
                        int count = 0;
//                    blank = 0;
//                    content = new HashMap<>();
                        while (linha != null) {

                            System.out.println(linha);

                            content.add(linha);
                            String[] palavras = linha.split(" ");

                            for (int i = 0; i < palavras.length; i++) {
                                int ngram = 3;
                                int ngramI = 0;
//                            while (ngramI < ngram && ngramI < palavras.length) {

                                String palavra = "";
                                while (ngramI < ngram) {
                                    if ((i + ngramI) >= palavras.length) {
                                        break;
                                    }
                                    if (palavra.equals("")) {
                                        palavra = palavras[i + ngramI];
                                    } else {
                                        palavra = palavra + " " + palavras[i + ngramI];
                                    }
                                    System.out.println(palavra);
                                    ngramI++;

                                    palavra = palavra.toLowerCase();

                                    if (!palavra.equals("") && !palavra.equals(" ")) {
                                        for (String caractere : paradigmatic.Caracteres.getCaracteres()) {

                                            if (caractere.equals("?")) {
                                                palavra = palavra.replaceAll("[\\" + caractere + "]", "");
//                                        System.out.println("LINHA_: " + linha);
                                            } else {
                                                palavra = palavra.replaceAll("[\\" + caractere + "]", "");
//                                        System.out.println("LINHA: " + linha);
                                            }
                                        }
                                    }

                                    if (this.palavraDocs.containsKey(palavra)) {

                                        List l = palavraDocs.get(palavra);
                                        l.add(quantidadeDoc);
                                        this.palavraDocs.put(palavra, l);

                                    } else if (StopWord.getStopwords().containsKey(palavra)) {
                                        count--;
                                    } else {
                                        String[] ps = palavra.split(" ");
                                        if (ps.length == 2) {
                                            System.out.println("PS: " + ps[0] + " " + ps[1]);
                                            if (!StopWord.getStopwords().containsKey(ps[0]) && !StopWord.getStopwords().containsKey(ps[1])) {
                                                List<Integer> l = new ArrayList<>();
                                                l.add(quantidadeDoc);
                                                this.palavraDocs.put(palavra, l);
//                                chaves.add(palavra);
                                                aux++;
//                                            System.out.println("2");
//                                            System.exit(0);
                                            }
                                        } else if (ps.length == 3) {
                                            System.out.println("PS: " + ps[0] + " " + ps[1] + " " + ps[2]);
                                            if ((!StopWord.getStopwords().containsKey(ps[0]) && !StopWord.getStopwords().containsKey(ps[1]) && !StopWord.getStopwords().containsKey(ps[2])) || (!StopWord.getStopwords().containsKey(ps[0]) && StopWord.getStopwords().containsKey(ps[1]) && !StopWord.getStopwords().containsKey(ps[2]))) {
                                                List<Integer> l = new ArrayList<>();
                                                l.add(quantidadeDoc);
                                                this.palavraDocs.put(palavra, l);
//                                chaves.add(palavra);
                                                aux++;
                                                if (palavra.equals("of santo")) {
                                                    System.out.println("kamspdkaskdmaskpdmapkdm");
                                                }
//                                            System.out.println("3");
//                                            System.exit(0);
                                            }
                                        } else {
                                            List<Integer> l = new ArrayList<>();
                                            l.add(quantidadeDoc);
                                            this.palavraDocs.put(palavra, l);
//                                chaves.add(palavra);
                                            aux++;
                                        }

                                    }

                                    if (docPalavra.containsKey(palavra)) {
                                        Map<Integer, Integer> m = docPalavra.get(palavra);
                                        m.put(quantidadeDoc, count);
                                        docPalavra.put(palavra, m);
                                    } else {
                                        Map<Integer, Integer> m = new HashMap<>();
                                        m.put(quantidadeDoc, count);
                                        docPalavra.put(palavra, m);
                                    }
                                }

//                            }
                                count++;
                            }

                            palavrasPorDoc.put(quantidadeDoc, aux);
                            quantidadeDoc++;
                            linha = lerArq.readLine();
                        }

                        preProcess();

                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(Summarizer.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Summarizer.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Summarizer.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        try {
                            lerArq.close();
                        } catch (IOException ex) {
                            Logger.getLogger(Summarizer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    //break;
                }

            }

        }

//        System.out.println("title: " + title);
    }

    public void preProcess() {

        StringBuilder sb = new StringBuilder();
        construirMatrizFrequencia();

    }

    public void construirMatrizFrequencia() {
        for (Map.Entry<String, List<Integer>> entrySet : palavraDocs.entrySet()) {
            String key = entrySet.getKey();
            List<Integer> value = entrySet.getValue();

            if (palavraDocs.get(key).size() > 1) {
                chaves.add(key);
            }
        }

        matrizFrequencia = new MatrizFrequencia(chaves.size(), quantidadeDoc);

        int aux = 0;
        for (String chave : chaves) {
            for (Integer value : palavraDocs.get(chave)) {

                double v = matrizFrequencia.get(aux, value) + 1.0;
                matrizFrequencia.set(aux, value, v);
            }
            aux++;
        }

        System.out.println("Matriz Frequencia:");
        matrizFrequencia.print(0, 3);

        calcularTFIDF();
    }

    public void calcularTFIDF() {

        int i = 0;
        int qtdd = 0;
        for (String c : chaves) {

            for (int j = 0; j < quantidadeDoc; j++) {

//                System.out.println(matrizFrequencia.get(i, j));
//                System.out.println((double) palavrasPorDoc.get(j));
//                System.out.println((double) quantidadeDoc);
//                System.out.println((double) palavraDocs.get(chaves.get(i)).size());
//                System.out.println("Chave: " + c);
                matrizFrequencia.set(i, j, TFIDF.calculoTFIDF(matrizFrequencia.get(i, j), (double) palavrasPorDoc.get(j), (double) quantidadeDoc, (double) docPalavra.get(c).size()));
            }
            i++;
        }
        System.out.println("");
        System.out.println("");
        System.out.println("");
        matrizFrequencia.print(0, 3);

        computingScore();
    }

    public void computingScore() {

        FileWriter arqW = null;
        try {
            Map<String, Double> words = new HashMap<>();
            Map<Integer, Double> docs = new HashMap<>();
            Double vW = 0.0, vD = 0.0;
            for (int linha = 0; linha < chaves.size(); linha++) {
                vW = 0.0;
                for (int coluna = 0; coluna < quantidadeDoc; coluna++) {
                    vW += matrizFrequencia.get(linha, coluna);
                }

                words.put(chaves.get(linha), vW);
            }
            for (int linha = 0; linha < quantidadeDoc; linha++) {
                vD = 0.0;
                for (int coluna = 0; coluna < chaves.size(); coluna++) {
                    vD += matrizFrequencia.get(coluna, linha);
                }
                docs.put(linha, vD);
            }
            words = sortByValue(words);
            for (Map.Entry<String, Double> entrySet : words.entrySet()) {
                String key = entrySet.getKey();
                Double value = entrySet.getValue();

                System.out.println("Key: " + key + " Value: " + value);

            }
            docs = sortByValue(docs);
            for (Map.Entry<Integer, Double> entrySet : docs.entrySet()) {
                Integer key = entrySet.getKey();
                Double value = entrySet.getValue();

                System.out.println("Key: " + key + " Value: " + value);

            }
            arqW = new FileWriter(local.substring(0, local.length() - 4)+"_resumo", false);
            PrintWriter gravarArq = new PrintWriter(arqW);

            List<Integer> pos = new ArrayList<>();
            int qt = 0;
            for (Map.Entry<Integer, Double> entrySet : docs.entrySet()) {
                Integer key = entrySet.getKey();
                Double value = entrySet.getValue();

                pos.add(key);
                if(MAX_SENTENCES < docs.size())
                if (qt + 1 == MAX_SENTENCES) {
                    break;
                }
                
                qt++;
            }
            
            Collections.sort(pos);
            
            for (Integer po : pos) {
                gravarArq.println(content.get(po));
            }
            
            arqW.close();
        } catch (IOException ex) {
            Logger.getLogger(Summarizer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                arqW.close();
            } catch (IOException ex) {
                Logger.getLogger(Summarizer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static <K, V extends Comparable<? super V>> Map<K, V>
            sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list
                = new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public static void main(String[] args) {

        //String dir = "C:/Users/Jario/Documents/resumos";
        String dir = "C:\\Users\\Jario\\Documents\\DUC 2002 Clusters";

        Summarizer s = new Summarizer();

        s.loadFile(dir);
        //s.preProcess();
    }
}
