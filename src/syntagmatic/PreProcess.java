/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntagmatic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jario
 */
public class PreProcess {

    private Integer quantidadeDoc;
    private Map<String, Integer> palavras;
    
    public PreProcess(){
        palavras = new HashMap<>();
    }
    
    public void preT() {

        try {
            String dirBase = "stopwords_en.txt";
            File dirBaseP = new File(dirBase);
            
            FileReader arq = new FileReader(dirBaseP);
            
            BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(dirBaseP), "UTF-8"));
            String linha = lerArq.readLine();
            
            while (linha != null) {
                
                System.out.println("stopWords.put(\""+linha+"\",\"\");");
                linha = lerArq.readLine();
            }
            
            arq.close();
        } catch (IOException ex) {
            Logger.getLogger(PreProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
   
    }
    
    public static void main(String[] args) {
        PreProcess pp = new PreProcess();
        
        pp.preT();
    }
}
